package main

/**
 * La estructura con el tipo de respuesta que entregaremos cuando sea consumida nuestra API
 */
type Respuesta struct {
	// Los nombres de posiciones o campos dentro del struct deben iniciar con la primera letra mayúscula
	Posicion struct {
		// De igual forma en un struct de structs debemos respetar la nomenclatura de mayúsucla
		X float64 `json:"x"`
		Y float64 `json:"y"`
	} `json:"position"`
	Mensaje string `json:"message"`
}

/**
 * Satelites struct que contiene un arreglo de Satélites
 */
type Satelites struct {
    Satelites []Satelite `json:"satellites"`
}

/**
 * Satelite struct que contiene los datos específicos enviados a un satélite en particular
 * Nombre {string} Nombre del satélite (Kenobi, Sato, etc...)
 * Distancia {float64} Distancia de la nave al satélite
 * Mensaje {[]string} Un arreglo con las palabras que forman el posible mensaje enviado por la nave al satélite
 */
type Satelite struct {
    Nombre string `json:"name"`
	Distancia float64 `json:"distance"`
	Mensaje []string `json:"message"`
}

type Datos struct {
	Id    int64  
	Nombre  string 
	Distancia float64 
	Mensaje  string
}
