package main

import (
	"fmt"
	"net/http"
	"encoding/json"
	"strings"
    "github.com/gorilla/mux"
)

// NOTA 1: Las acciones en este archivo echan mano de structs que se encuentran en el archivo structs.go
// NOTA 2: Las firmas solicitadas en el challenge pueden ser encontradas en el archivo firmas.go
// NOTA 3: Las acciones en este archivo echan mano de distintas funciones para cumplir la lógica y se encuentran en el archivo functions.go

const st_a = "kenobi"; 
const st_b = "skywalker"; 
const st_c = "sato"; 

/**
 * topSecret
 * Obtiene la ubicación de la nave y el mensaje que emite.
 *
 * @param  {http.ResponseWriter} w Objeto con el cual se construye la respuesta dada por la API
 * @param  {http.Request} r        Objeto que contiene los datos de la solicitud
 *
 * @return {mixed} en caso de éxito Entrega la respuesta json con las coordendas de la nave y su mensaje limpio y claro
 *
	El formato esperado por esta acción dentro de su body es el siguiente:
	{
		"satellites": [
			{
				"name": "kenobi",
				"distance": 485.6956,
				"message": ["este", "", "", "mensaje", ""]
			},
			.....
		]
	}
 * @author J. Damián Jiménez Navarro <jdamianjm@gmail.com>
 */
func topSecret(w http.ResponseWriter, r *http.Request){
	// Variable de tipo Satelites
	var satelites Satelites
	// Decodificación del json dentro del body de la petición
	decoder := json.NewDecoder(r.Body)
	// La decodificación la asignamos al struct representado por la variable satelites
	decoder.Decode(&satelites)


	// Variables donde recolectamos tanto las distancias como los mensajes enviados a los 3 diferentes satélites
	distancias_recuperadas 	:= make([]float64, 3)
	mensajes_recuperados 	:= make([][]string, 3)

	// Si el arreglo está vacío es porque se debe a que hay algo mal con los datos de entrada, entonces mandamos error 404
	if len(satelites.Satelites) == 0 {
		deliverStringResponse(w, 404, "No se puede determinar la posición o el mensaje, favor de verificar")
		return
	}

	t:= 0
	for i := 0; i < len(satelites.Satelites); i++ {
	    fmt.Println("Nombre satélite: " + satelites.Satelites[i].Nombre)

	    if strings.ToLower(satelites.Satelites[i].Nombre) == st_a {
			t = 0 
		} else if strings.ToLower(satelites.Satelites[i].Nombre) == st_b {
			t = 1 
		} else {
			t = 2
		}
	    distancias_recuperadas[t] = float64(satelites.Satelites[i].Distancia)
	    mensajes_recuperados[t] = satelites.Satelites[i].Mensaje
	}

	fmt.Println(distancias_recuperadas)

	// Envío de mensajes para obtener el mensaje limpio y claro
	mensaje_resultado := getMessage(mensajes_recuperados[0], mensajes_recuperados[1], mensajes_recuperados[2])

	// Envío de distancias para realizar el cálculo de trilateración
	x, y := getLocation(distancias_recuperadas[0], distancias_recuperadas[1], distancias_recuperadas[2])

	// Si el mensaje es cadena vacía es porque el mensaje no se pudo determinar pues puede existir alguna inconsistencia con la entrada del mismo, entonces mandamos error 404
	if mensaje_resultado == "" {
		deliverStringResponse(w, 404, "No se puede determinar la posición o el mensaje, favor de verificar")
		return
	}

	fmt.Println(mensaje_resultado)
	fmt.Println(x)
	fmt.Println(y)

	res := new(Respuesta) // dot notation
	res.Posicion.X = x
	res.Posicion.Y = y
	res.Mensaje = mensaje_resultado

	deliverResponse(w, 200, res)
}

/**
 * topSecretSplit
 * Recibe la distancia y el mensaje para cada satélite, identificándolo por su nombre
 * Si todo marcha bien los persiste en una base de datos, para que con ello la función topSecretSplitGet pueda establecer las coordenadas de la nave y su mensaje
 *
 * @param  {http.ResponseWriter} w Objeto con el cual se construye la respuesta dada por la API
 * @param  {http.Request} r        Objeto que contiene los datos de la solicitud
 *
 * @return {mixed} Retorno de mensaje para notificar al usuario si pudo o no ser guardado y/o actualizado el registro
 *
 * El formato esperado por esta acción dentro de su body es el siguiente:
	{
		"distance": 485.6956,
		"message": ["este", "", "", "mensaje", ""]
	}
 *
 * @author J. Damián Jiménez Navarro <jdamianjm@gmail.com>
 */
func topSecretSplit(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	satellite_name := params["satellite_name"]
	fmt.Println(satellite_name)

	// Variable de tipo Satelite donde almacenamos el body recibido
	var satelite Satelite
	// Decodificación del json dentro del body de la petición
	decoder := json.NewDecoder(r.Body)
	// La decodificación la asignamos al struct representado por la variable satelite
	decoder.Decode(&satelite)

	fmt.Println(satelite.Distancia)
	fmt.Println(satelite.Mensaje)

	db, err := getDB()
	if err != nil {
		fmt.Println(err)
	}

	// Buscamos registro coincidente con el nombre del satélite dado
	var num_data int64
	rows_count, err := db.Query("SELECT COUNT(*) AS num_data FROM spaceship_data WHERE name= ?", satellite_name)
	if err != nil {
		fmt.Println(err)
	} else {
		for rows_count.Next() {
			rows_count.Scan(&num_data)
		}
	}
	fmt.Println(num_data)

	if num_data == 0 {
		// Si no encontramos datos con el nombre del satélite, entonces procedemos a insertar un nuevo registro, de lo contrario actualizamos 
		_, err = db.Exec("INSERT INTO spaceship_data (name, distance, message) VALUES (?, ?, ?)", satellite_name, satelite.Distancia, strings.Join(satelite.Mensaje, ","))
	} else {
		// En caso de encontrar el registro, procedemos a sólo actualizarlo
		_, err = db.Exec("UPDATE spaceship_data SET distance = ?, message = ? WHERE name = ?", satelite.Distancia, strings.Join(satelite.Mensaje, ","), satellite_name)
	}

	if err == nil {
		deliverStringResponse(w, 200, "Datos almacenados y/o actualizados correctamente")
		return
	} else {
		deliverStringResponse(w, 404, "Los datos no pudieron ser almacenados y/o actualizados, favor de verificar")
		fmt.Println(err);
		return
	}
}

/**
 * topSecretSplitGet
 * Obtiene la ubicación de la nave y el mensaje que emite, leyendo los datos almacenados por topSecretSplit en base de datos
 *
 * @param  {http.ResponseWriter} w Objeto con el cual se construye la respuesta dada por la API
 * @param  {http.Request} r        Objeto que contiene los datos de la solicitud
 *
 * @return {mixed} en caso de éxito Entrega la respuesta json con las coordendas de la nave y su mensaje limpio y claro
 *
 * @author J. Damián Jiménez Navarro <jdamianjm@gmail.com>
 */
func topSecretSplitGet(w http.ResponseWriter, r *http.Request) {
	// Obtenemos conexión a bd
	db, err := getDB()
	if err != nil {
		fmt.Println(err)
	}

	// Comprobamos que haya información persistida en la bd sobre las distancias de la nave con respecto a los 3 satélites
	var num_data int64
	rows_count, err := db.Query("SELECT COUNT(*) AS num_data FROM spaceship_data")
	if err != nil {
		fmt.Println(err)
	} else {
		for rows_count.Next() {
			rows_count.Scan(&num_data)
		}
	}
	fmt.Println(num_data)

	// Si el número de registros es menor a 3 es que no hay suficiente información y detenemos el proceso
	if num_data < 3 {
		deliverStringResponse(w, 404, "No hay suficiente información, favor de verificar y/o agregar la información de las distancias para los 3 diferentes satélites, se puede hacer consumiendo el servicio topsecret_split/{satellite_name}")
		return
	}

	// Variables donde recolectamos tanto las distancias como los mensajes enviados a los 3 diferentes satélites
	distancias_recuperadas 	:= make([]float64, 3)
	mensajes_recuperados 	:= make([][]string, 3)
	
	// Obtención de los registros de distancia mandados por la nave a los 3 diferentes satélites
	rows, err := db.Query("SELECT id, LOWER(name), distance, message FROM spaceship_data")
	if err != nil {
		fmt.Println(err)
	}

	i := 0;
	for rows.Next() {
		// Guardado temporal de los registros obtenidos desde bd, para ser enviados a procesar con Trilateración y manipulación del mensaje
		var data Datos
		err = rows.Scan(&data.Id, &data.Nombre, &data.Distancia, &data.Mensaje)
		if err != nil {
			fmt.Println(err)
		}

		if data.Nombre == st_a {
			i = 0 
		} else if data.Nombre == st_b {
			i = 1 
		} else {
			i = 2
		}

		distancias_recuperadas[i] = float64(data.Distancia)
	    mensajes_recuperados[i] = strings.Split(data.Mensaje, ",")
	}

	// Envío de mensajes para obtener el mensaje limpio y claro
	mensaje_resultado := getMessage(mensajes_recuperados[0], mensajes_recuperados[1], mensajes_recuperados[2])

	// Envío de distancias para realizar el cálculo de trilateración
	x, y := getLocation(distancias_recuperadas[0], distancias_recuperadas[1], distancias_recuperadas[2])

	// Si el mensaje es cadena vacía es porque el mensaje no se pudo determinar pues puede existir alguna inconsistencia con la entrada del mismo, entonces mandamos error 404
	if mensaje_resultado == "" {
		deliverStringResponse(w, 404, "No se puede determinar la posición o el mensaje, favor de verificar")
		return
	}

	fmt.Println(mensaje_resultado)
	fmt.Println(x)
	fmt.Println(y)

	res := new(Respuesta) // dot notation
	res.Posicion.X = x
	res.Posicion.Y = y
	res.Mensaje = mensaje_resultado

	deliverResponse(w, 200, res)
}