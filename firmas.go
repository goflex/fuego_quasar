package main

import (
    "strings"
    "math"
)

// ########## Firmas (necesarias) que deben ser contempladas para el Challenge MELI MX - Start ##########

/**
 * getLocation
 * Por medio de las distancias recolectadas por los 3 diferentes satélites, obtiene las coordenadas (x,y) de la nave 
 *
 * @param  {float64 variadic} distances ...float64 Parámetro variadic (variádico) o también conocido como Listas de argumentos de longitud variable
 * donde recibimos las distancias dadas por la nave a los diferentes satélites 
 *
 * @return {float64} Retorna las coordenadas x e y con la posición de la nave
 *
 * @author J. Damián Jiménez Navarro <jdamianjm@gmail.com>
 */
func getLocation(distances ...float64) (x, y float64){
	// Kenobi coordenadas
	y1 := -200.0
    x1 := -500.0
    // Skywalker coordenadas
    y2 := -100.0
    x2 := 100.0
    // Sato coordenadas
    y3 := 100.0
    x3 := 500.0
    // Distancias
    r1 := distances[0]
    r2 := distances[1]
    r3 := distances[2]

    // Para obtener las coordenadas de la nave usamos TRILATERACIÓN
    A := 2*x2 - 2*x1;
    B := 2*y2 - 2*y1;
    C := math.Pow(r1, 2) - math.Pow(r2, 2) - math.Pow(x1, 2) + math.Pow(x2, 2) - math.Pow(y1, 2) + math.Pow(y2, 2);
    D := 2*x3 - 2*x2;
    E := 2*y3 - 2*y2;
    F := math.Pow(r2, 2) - math.Pow(r3, 2) - math.Pow(x2, 2) + math.Pow(x3, 2) - math.Pow(y2, 2) + math.Pow(y3, 2);

    // Retornamos las coordenadas x, y de la nave!
	return (C*E - F*B) / (E*A - B*D), (C*D - A*F) / (B*D - A*E)
}

/**
 * getMessage
 * Por medio de los mensajes recolectadas por los 3 diferentes satélites, obtiene el posible mensaje limpio y claro dado por la nave
 * @param  {[]string variadic} messages ...[]string Parámetro variadic con arreglo de mensajes enviados desde la nave a cada satélite
 *
 * @return {string} Posible mensaje limpio y claro
 *
 * @author J. Damián Jiménez Navarro <jdamianjm@gmail.com>
 */
func getMessage(messages ...[]string) (msg string){
	// Mensaje recuperado
	retrieved_msg := make([]string, 0)

	for _, message := range messages {
		for j, msg_sent := range message {
		    if len(retrieved_msg) > j {
				if msg_sent != "" { retrieved_msg[j] = msg_sent }
			} else {
				retrieved_msg = append(retrieved_msg, msg_sent)
			}
	    }
	}

	// Conjuntamos todos los posibles mensajes en uno solo, el cual debería ser el que esté claro y limpio
	return strings.Join(retrieved_msg, " ")
}
// ########## Firmas (necesarias) que deben ser contempladas para el Challenge MELI MX - End ##########