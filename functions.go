package main

import (
	"net/http"
	"encoding/json"
	"database/sql"
    _ "github.com/ziutek/mymysql/godrv"

)

/**
 * getDB
 * Setea los datos para la conexión a base de datos
 *
 * @return Conexión a base de datos mysql
 *
 */
func getDB() (*sql.DB, error) {
	const db_user 		= "nsstore_phpmyadmin"
    const db_name 		= "nsstore_phpmyadmin"
    const db_password 	= "Kuna9579$s"
    const ip 	= "tcp:nsstore.mx:3306"

	return sql.Open("mymysql", ip + "*" + db_name + "/" + db_user + "/" + db_password)
}

/**
 * deliverResponse 
 * Genera el header, el código de status especificado y codifica tipo JSON los datos ingresados desde el parámetro results
 * para entregar respuesta cuando alguien consuma los servicios de esta API Rest
 * @param  {http.ResponseWriter}   	w       Objeto con el cual se construye la respuesta dada por la API
 * @param  {int}   					status  Número para indicar el código de respuesta dado por el server o API (200, 404, 500, etc)
 * @param  {*Respuesta}   			results Struct tipo respuesta el cual contiene los campos de Posiciones y Mensaje entregado           
 * @return {json} Estructura tipo JSON entregada como respuesta al usuario
 * @date   2021-08-18
 * @author J. Damián Jiménez Navarro <jdamianjm@gmail.com>
 */
func deliverResponse(w http.ResponseWriter, status int, results *Respuesta){
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(results)
}

/**
 * deliverStringResponse
 * Genera el header, el código de status especificado y retorna respuesta tipo json
 *
 * @param  {http.ResponseWriter}   	w       Objeto con el cual se construye la respuesta dada por la API
 * @param  {int}   					status  Número para indicar el código de respuesta dado por el server o API (200, 404, 500, etc)
 * @param  {string} 				msg     Mensaje a mostrar
 *
 * @return {json} JSON simple con mensaje entregado como respuesta al usuario
 *
 * @author J. Damián Jiménez Navarro <jdamianjm@gmail.com>
 */
func deliverStringResponse(w http.ResponseWriter, status int, msg string){
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(msg)
}