package main

import(
	"net/http"
	"github.com/gorilla/mux"
)

/**
 * Tipo de dato definido y personalizado llamado Ruta, que tien como utilidad almacenar la colección de datos para la ruta de cada servicio expuesto
 */
type Ruta struct{
	Nombre string
	Metodo string
	Ruta string
	Manejador http.HandlerFunc
}

/**
 * Colección de rutas a ser manejadas por el sistema
 */
type Rutas []Ruta

func NewRouter() *mux.Router {
	enrutador := mux.NewRouter().StrictSlash(true)

	/* Seteo de las rutas almacenadas en la variable "rutas" dentro del router provisto por gorilla mux, 
	esto con la finalidad de que estén disponibles de cara al usuario y pueda consumirlas */
	for _, ruta := range rutas{
		enrutador.Methods(ruta.Metodo).
			Path(ruta.Ruta).
			Name(ruta.Nombre).
			Handler(ruta.Manejador)
	}

	return enrutador
}

var rutas = Rutas{
	Ruta{
		"topSecret",
		"POST",
		"/topsecret",
		topSecret,
	},
	Ruta{
		"topSecretSplit",
		"POST",
		"/topsecret_split/{satellite_name}",
		topSecretSplit,
	},
	Ruta{
		"topSecretSplit",
		"GET",
		"/topsecret_split",
		topSecretSplitGet,
	},
}