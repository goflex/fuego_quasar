# Challenge MELI MX - Operación fuego quasar

Aquí se almacena el código escrito en Go que cubre el Challenge para MELI MX

[Dar clic para ver video con la explicación del consumo de esta API REST](https://www.youtube.com/watch?v=S8xySTkB6HI)

## Herramientas utilizadas

- Golang (1.17)
- MariaDB (5.5.5-10.1.48)
- Servicios hospedados en Google Cloud
- Base dedatos hospedada en dominio y host propio

## Pasos para ejecutar sistema

- Clonar repo
```
git clone [url_de_este_repo]
```
- Entrar a la carpeta del proyecto ya descargada e inicializar el proyecto
```
go mod init gitlab.com/goflex/fuego_quasar
```
- Descargar dependencias:
```
go get github.com/gorilla/mux
go get github.com/ziutek/mymysql/godrv

```
- El sistema echa mano de una base de datos mysql, por lo cual se debe crear una base con una tabla dentro llamada **spaceship_data**, La tabla debe tener la siguiente estructura:

| Field | Type | Null | Key | Extra |
| ----- | ---- | ---- | --- | ----- |
| id | int(11) | NO | PRI | auto_increment |
| name | varchar(250) | NO | PRI |  |
| distance | decimal(19,6) | NO |  |  |
| message | text | NO |  |  |


## Solución a la problemática propuesta

Independientemente del lenguaje de programación, se empleó para dar solución al problema, una técnica geométrica llamada **TRILATERACIÓN**, en la cual en base a 3 puntos que en este caso son los satélites (Kenobi, Skywalker y Sato), más las distancias a las cuales está la nave con respecto a cada uno de ellos, podemos encontrar entonces las COORDENADAS de dicha nave, pudiendo así resolver el challenge.

## Exposición de endpoints

**Ruta principal**
https://quasargo.appspot.com

Todos los métodos o endpoints publicados en la anterior ruta están protegidos, por lo cual para consumirlos deberás agregar una API KEY, esto, con el objetivo de contemplar una capa básica de seguridad en el sistema.

La API KEY es la siguiente:
```
?key=AIzaSyDFS8dN2PiZdQ5V_3CnxRgp8Wl2yhdnSyg
```

_**topsecret**_ (POST)

https://quasargo.appspot.com/topsecret?key=AIzaSyDFS8dN2PiZdQ5V_3CnxRgp8Wl2yhdnSyg

ejemplo del body recibido:

```
{
	"satellites": [
		{
			"name": "kenobi",
			"distance": 485.6956,
			"message": ["este", "", "", "mensaje", ""]
		},
		{
			"name": "skywalker",
			"distance": 266.0831,
			"message": ["", "es", "", "", "secreto"]
		},
		{
			"name": "sato",
			"distance": 600.5,
			"message": ["este", "", "un", "", ""]
		}
	]
}
```

_**topsecret_split/{satellite_name}**_ (POST)

https://quasargo.appspot.com/topsecret_split/kenobi?key=AIzaSyDFS8dN2PiZdQ5V_3CnxRgp8Wl2yhdnSyg

ejemplo del body recibido:

```
{
	"distance": 485.6956,
	"message": ["este", "", "", "mensaje", ""]
}
```

_**topsecret_split**_ (GET)

https://quasargo.appspot.com/topsecret_split?key=AIzaSyDFS8dN2PiZdQ5V_3CnxRgp8Wl2yhdnSyg


## Material adjunto

**Archivo de colección de endpoints**
Se recomienda realizar el consumo de las APIS en POSTMAN.
Para mayor comodida en el consumo de los endpoints, pongo a su disposición el siguiente archivo json que pueden importar dentro de su POSTMAN, el cual creará en automático la colección con los 3 diferentes endpoints de este sistema, listos para ser consumidos!:

[Archivo con endpoints](https://drive.google.com/file/d/1vzS5LKyxw3kW0wMyR5UQ8JsvXNxbC0B0/view?usp=sharing)

**Archivo sobre documentación completa de la solución desarrollada**
[Archivo con documentación](https://drive.google.com/file/d/1mb6FbG6Slg1LFR-rL6CGm9PbqLWjgWHK/view?usp=sharing)



